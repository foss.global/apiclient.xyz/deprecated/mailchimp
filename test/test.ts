import { expect, tap } from 'tapbundle'
import * as mchimp from '../dist/index'
import { Qenv } from 'qenv'

let testQenv = new Qenv(process.cwd(), process.cwd() + '/.nogit')

let myMchimpAccount: mchimp.MchimpAccount

tap.test('should create a valid MchimpAccount', async () => {
  myMchimpAccount = new mchimp.MchimpAccount()
  myMchimpAccount.auth(process.env.MCHIMP_KEY)
  expect(myMchimpAccount).to.be.instanceof(mchimp.MchimpAccount)
})

tap.test('get lists', async () => {
  let lists = await myMchimpAccount.getLists()
  console.log(lists)
})

tap.test('getListWithName', async () => {
  let sandboxList = await myMchimpAccount.getListWithName('Sandbox')
  expect(sandboxList.name).to.equal('Sandbox')
})

tap.test('should get interest groups for a list', async () => {
  let interests = await myMchimpAccount.getInterestsForList('Sandbox')
  console.log(interests)
})

tap.test('subscribe someone to a list', async () => {
  await myMchimpAccount.smartsubscribe(
    'Sandbox',
    'sandbox@lossless.com',
    'subscribed' , {
      FNAME: 'Peter',
      LNAME: 'Miller'
    },
    ['group1']
  )
})
tap.start()
