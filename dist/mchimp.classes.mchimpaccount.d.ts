export declare class MchimpAccount {
    private _datacenter;
    private _authBase64;
    constructor();
    /**
     * authenticate
     */
    auth(keyArg: string): void;
    /**
     * smartsubscribe to a list
     */
    smartsubscribe(listNameArg: string, mailAdressArg: string, statusArg: string, inputObjectArg: any, interestArrayArg: string[]): Promise<void>;
    getLists(): Promise<any>;
    getListWithName(nameArg: string): Promise<any>;
    getInterestsForList(listNameArg: string): Promise<any[]>;
    request(methodArg: any, routeArg: string, dataArg?: {}): Promise<any>;
}
